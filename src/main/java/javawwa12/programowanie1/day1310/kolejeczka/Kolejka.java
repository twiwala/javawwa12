package javawwa12.programowanie1.day1310.kolejeczka;

public class Kolejka {
    private int [] tab;

    private int pushIndex = 0;
    private int popIndex = 0;

    private int size = 0;

    public Kolejka(int capacity){
        tab = new int[capacity];
    }

    public void push(int value){
        if(size == tab.length){
            throw new KolejkaFullException("Size == tab.length");
        }
        //aby nie wyjechac poza zakres tablicy :)
        tab[(pushIndex++ % tab.length)] = value;
        size++;
    }
    public int pop(){
        if(size == 0){
            throw new KolejkaEmptyException("Kolejka is empty :((");
        }
        int result = tab[popIndex++ % tab.length];
        size--;
        return result;
    }
/*
    public int pop(){
        if(size == 0){
            throw new KolejkaEmptyException("Kolejka is empty :((");
        }
        int result = tab[popIndex];
        popIndex++;
        size--;
        if(popIndex == tab.length){
            popIndex = 0;
        }
        return result;
    }
 */
    public static void main(String[] args) {
        Kolejka kolejka = new Kolejka(4);
        kolejka.push(1);
        kolejka.push(2);
        kolejka.push(3);
        kolejka.push(4);
        System.out.println(kolejka.pop()); // 1
        kolejka.push(5);

        System.out.println(kolejka.pop()); // 2
        System.out.println(kolejka.pop()); // 3
        System.out.println(kolejka.pop()); // 4
        System.out.println(kolejka.pop()); // 5
//        Kolejka kolejka = new Kolejka(3);
//        kolejka.push(1);
//        kolejka.push(2);
//        kolejka.push(3);
//        System.out.println(kolejka.pop());
//        System.out.println(kolejka.pop());
//        kolejka.push(4);
//        System.out.println(kolejka.pop());
//        System.out.println(kolejka.pop());
    }
}
