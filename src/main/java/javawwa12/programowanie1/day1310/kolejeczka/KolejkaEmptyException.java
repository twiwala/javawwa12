package javawwa12.programowanie1.day1310.kolejeczka;

public class KolejkaEmptyException extends RuntimeException {
    public KolejkaEmptyException(String message) {
        super(message);
    }
}
