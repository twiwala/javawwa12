package javawwa12.programowanie1.day1310.kolejeczka;

public class KolejkaFullException  extends RuntimeException {
    public KolejkaFullException(String message) {
        super(message);
    }
}
