package javawwa12.programowanie1.day1310;

public class Wyszukiwanie {
    public static void main(String[] args) {
        int[] tab1 = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] tab2 = {1, 3, 2, 1, 5, 6, 1, 2};
        int[] tab3 = {5, 2, 1, 2, 5, 6, 8, 9, 1, 2};

        System.out.println(wyszukiwanieLiniowe(tab1, 5));
        System.out.println(wyszukiwanieLiniowe(tab1, 15));

        System.out.println(wyszukiwanieBinarne(tab1, 7));
    }

    public static boolean wyszukiwanieBinarne(int [] tab, int element){
        int lewy = 0;
        int prawy = tab.length - 1;

        int counter = 0;

        while(lewy <= prawy){
            counter++;
            int srodek = (lewy + prawy ) / 2;

            if(tab[srodek] == element){
                System.out.println(counter);
                return true;
            } else if(tab[srodek] < element){
                lewy = srodek + 1;
            } else {
                prawy = srodek - 1;
            }

        }
        return false;
    }

    public static boolean wyszukiwanieLiniowe(int[] tab, int element) {
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == element) {
                System.out.println(i);
                return true;
            }
        }
        System.out.println(tab.length);
        return false;
    }
}
