package javawwa12.programowanie1.day1310;

public class Zad1Silnia {
    public static void main(String[] args) {
        System.out.println(silniaIteracyjnie(5));
        System.out.println(silniaIteracyjnie(2));
        System.out.println(silniaIteracyjnie(0));

        System.out.println(silniaRek(5));
        System.out.println(silniaRek(2));
        System.out.println(silniaRek(3));
        System.out.println(silniaRek(0));

    }

    public static int silniaIteracyjnie(int n) {
        int iloczyn = 1;

        for (int i = 2; i <= n; i++) {
            iloczyn *= i;
        }
        return iloczyn;
    }

    public static int silniaRek(int n){
        if(n <= 1){
            return 1;
        }
        return n * silniaRek(n-1);
    }
}
