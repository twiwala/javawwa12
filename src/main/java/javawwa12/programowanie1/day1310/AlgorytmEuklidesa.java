package javawwa12.programowanie1.day1310;

public class AlgorytmEuklidesa {
    public static void main(String[] args) {
        System.out.println(NWD(10, 3));
        System.out.println(NWD(24, 8));
    }
    public static int NWD(int a, int b){
        while(a!=b){
            if(a > b){
                a -= b;
            } else {
                b -= a;
            }
        }
        return a;
    }
}
