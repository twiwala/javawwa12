package javawwa12.programowanie1.day1310.stosik;

public class Stackk {
    private int[] tab;
    private int size;

    public Stackk(int capacity) {
        tab = new int[capacity];
    }

    public void push(int value) {
        if(size == tab.length){
            throw new StackFullException("Size is full! You have to pop first!!!!");
        }
        tab[size++] = value;
    }

    public int pop() {
        if(size == 0){
            throw new StackEmptyException("Size is: 0");
        }
        return tab[--size];
    }

    /**
     * ilosc elementow na stosie
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    public static void main(String[] args) {
        Stackk stackk = new Stackk(4);
        try {
            stackk.push(3);
            stackk.push(4);
            stackk.push(0);
            stackk.push(0);
            stackk.push(0);
        } catch(StackFullException e){
            System.out.println("Stos jest pelen");
        }
        System.out.println(stackk.pop()); //0
        System.out.println(stackk.pop()); //4
        System.out.println(stackk.pop()); //3

    }
}
