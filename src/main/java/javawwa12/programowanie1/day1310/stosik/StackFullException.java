package javawwa12.programowanie1.day1310.stosik;

public class StackFullException extends RuntimeException {
    public StackFullException(String message) {
        super(message);
    }
}
