package javawwa12.programowanie1.day1310.stosik;

public class StackEmptyException extends RuntimeException {
    public StackEmptyException(String message) {
        super(message);
    }
}
