package javawwa12.programowanie1.day1310;

public class Fibonaci {
    public static void main(String[] args) {
        System.out.println(fibRek(5));

        System.out.println(fibRek(50));
        System.out.println(fibItTab(8));
        System.out.println(fibIt(8));
    }

    public static int fibIt(int n) {
        int a1 = 0;
        int a2 = 1;
        int tmp = 0;

        for (int i = 2; i <= n; i++) {
            tmp = a1 + a2;
            a1 = a2;
            a2 = tmp;
        }
        return tmp;

    }

    public static int fibItTab(int n) {
        int[] tab = new int[n + 1];
        tab[1] = 1;

        for (int i = 2; i <= n; i++) {
            tab[i] = tab[i - 1] + tab[i - 2];
        }
        return tab[n];
    }

    public static int fibRek(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        }
        return fibRek(n - 1) + fibRek(n - 2);
    }
}
