package javawwa12.programowanie1.day1310;

public class Banknoty {
    public static void main(String[] args) {
        int doWydania = 198;

        int ilosc20 = doWydania / 20;
        doWydania -= ilosc20 * 20;

        int ilosc10 = doWydania / 10;
        doWydania -= ilosc10 * 10;

        int ilosc5 = doWydania /5;
        doWydania -= ilosc5 * 5;

        int ilosc2 = doWydania / 2;
        doWydania -= ilosc2 * 2;

        int ilosc1 = doWydania;

        System.out.println("Banknotow 20: " + ilosc20);
        System.out.println("Banknotow 10: " + ilosc10);
        System.out.println("Banknotow 5: " + ilosc5);
        System.out.println("Banknotow 2: " + ilosc2);
        System.out.println("Banknotow 1: " + ilosc1);
    }
}
