package javawwa12.programowanie1.day0710.home;

/**
 * 3. Napisz program, który w tablicy dwuwymiarowej wskaże numer wiersza,
 * w którym jest najdłuższy ciąg jednakowych liczb.
 */
public class Zad3 {
    public static void main(String[] args) {

        int[][] tab = {
                {1, 2, 3, 3, 3}, //3
                {1, 2, 5, 8, 9}, //1
                {1, 3, 2, 3, 5}, //1
                {9, 8, 7, 6, 5} //1
        };

        int maxCiag = 1;
        int maxCiagWiersz = 0;
        for (int i = 0; i < tab.length; i++) {
            int ciag = najdluzszyCiag(tab[i]);
            if (maxCiag < ciag) {
                maxCiagWiersz = i;
                maxCiag = ciag;
            }
        }

        System.out.println(maxCiagWiersz);
    }

    public static int najdluzszyCiag(int[] tab) {
        //{1, 2, 3, 3, 3} // i = 4
        int licznik = 1;
        int maksymalnyLicznik = 1;

        for (int i = 0; i < tab.length - 1; i++) {
            if (tab[i] == tab[i + 1]) {
                licznik++;
            } else {
                licznik = 1;
            }
            if (licznik > maksymalnyLicznik) {
                maksymalnyLicznik = licznik;
            }
        }
        return maksymalnyLicznik;
    }
}
