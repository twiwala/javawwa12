package javawwa12.programowanie1.day0710.home;

/**
 * 14. Napisz program wyznaczający sumę wszystkich dwucyfrowych liczb naturalnych,
 * które przy dzieleniu przez daną liczbę naturalną k dają resztę r.
 */
public class Zad14 {
    public static void main(String[] args) {
        int k = 8;
        int r = 3;

        System.out.println(sumaDzielnikow(k, r));
    }

    public static int sumaDzielnikow(int k, int r) {
        int suma = 0;

        int i = r;
        while (i < 10) i += k;

        for (; i < 100; i += k) {
            suma += i;
        }
        return suma;
    }
}
