package javawwa12.programowanie1.day0710.home;

public class Zad5 {
    public static void main(String[] args) {
        int[][] szachownica = {
                {0, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 1},
        };

        System.out.println(czyJestBicie(szachownica));
    }

    public static boolean czyJestBicie(int[][] szachownica) {
        for(int i = 0 ; i < szachownica.length; i++){
            int sumaWWierszu = 0;
            int sumaWKolumnie = 0;
            for(int k = 0; k < szachownica[i].length; k++){
//                if(szachownica[i][k] == 1){
//                    if(bylaWieza){
//                        return true;
//                    } else {
//                        bylaWieza = true
//                    }
//                }
                sumaWWierszu += szachownica[i][k];
                sumaWKolumnie += szachownica[k][i];
                if(sumaWWierszu == 2 || sumaWKolumnie == 2){
                    return true;
                }
            }
        }
        return false;
    }
}
