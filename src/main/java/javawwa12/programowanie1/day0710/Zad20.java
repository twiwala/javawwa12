package javawwa12.programowanie1.day0710;

/**
 * 20.    Niech d(n) będzie definiowane jako suma dzielników liczby n
 * Np. dzielnikami liczby 220 są 1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110
 * D(220) = 284
 * Dzielnikami 284 są: 1,2,4,71, 142
 * D(284) = 220
 *
 * Liczby a i b są sobie przyjazne, jeżeli d(a) = b i d(b) = a i a !=b;
 * Znajdź sumę wszystkich przyjaznych liczb poniżej 10 000
 */
public class Zad20 {
    public static void main(String[] args) {
        int suma = 0;
        long start = System.currentTimeMillis();
        for(int a = 2; a < 10000; a++){
            int b = sumaDzielnikow(a);
            if(a != b && sumaDzielnikow(b) == a){
                suma += a;
            }
        }
        //... do something
        long stop = System.currentTimeMillis();
        System.out.println(stop-start);
        System.out.println(suma);
    }

    public static int sumaDzielnikow(int liczba){
        int suma = 1;
        for(int i = 2; i <= liczba/2;i++){
            if(liczba % i == 0){
                suma += i;
            }
        }
        return suma;
    }
}
