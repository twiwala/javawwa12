package javawwa12.programowanie1.day0710;

/**
 * 21.     Liczba 585 = 1001001001(2) jest palindromem w dwóch systemach.
 * Znajdź sumę wszystkich liczb, mniejszych od miliona które są palindromami w systemie 10 i 2. (Pomijamy zera wiodące)
 */
public class Zad21 {
    public static void main(String[] args) {
        //System.out.println(toNKB(585));

        int suma = 0;
        for (int i = 1; i < 1000000; i++) {
            if (isPalindrome(String.valueOf(i)) && isPalindrome(toNKB(i))) {
                suma += i;
            }
        }
        System.out.println(suma);
    }

    //ex1
    //in: 11
    //out: 1011

    //ex2:
    //in: 585
    //out: 1001001001
    public static String toNKB(int value) {

        StringBuilder stringBuilder = new StringBuilder();
        while (value > 0) {
            int digit = value % 2; // to 0 lub 1
            stringBuilder.append(digit);
            value /= 2;
        }
        return stringBuilder.reverse().toString();
    }

    //in: kajak
    //out: true

    //in: "1001001001"
    //out true
    public static boolean isPalindrome(String str) {
        int leftIndex = 0;
        int rightIndex = str.length() - 1;

        while (leftIndex < rightIndex) {
            if (str.charAt(leftIndex) != str.charAt(rightIndex)) {
                return false;
            }
            leftIndex++;
            rightIndex--;
        }
        return true;
    }
}
