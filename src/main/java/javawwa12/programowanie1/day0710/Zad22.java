package javawwa12.programowanie1.day0710;

/**
 * 22.    Przyjrzyjmy się liczbie 3797.
 * Jest interesująca, ponieważ możemy obcinać ją ciągle) od lewej do prawej, a ona ciągle będzie liczbą pierwszą.
 * 3797, 797, 97, 7
 * <p>
 * Podobnie możemy postąpić z (z tym, że zaczynamy od prawej do lewej)
 * 3797, 379, 37, 3
 * <p>
 * Znajdź sumę pierwszych 11 liczb które można ciąć od lewej do prawej, lub od prawej do lewej które w ten sposób cały czas będą pierwsze.
 * <p>
 * Uwaga: 2,3,5, 7 – nie powinny być rozważane jako obcinane liczby pierwsze.
 */
public class Zad22 {
    public static void main(String[] args) {

        int foundedValues = 0;
        int i = 8;

        int sum = 0;
        while(foundedValues < 11){
            if(isPrime(i) && (fromRightToLeft(i) && fromLeftToRight(i))){
                //System.out.println(i);
                foundedValues++;
                sum += i;
            }
            i++;
        }
        System.out.println(sum);
    }
    //3797 -> 3 -> 97 -> 797
    public static boolean fromLeftToRight(int value) {
        int modulo = 10; //10 -> 100 -> 1000

        while(value % modulo != value){
            if(!isPrime(value % modulo)){
                return false;
            }
            modulo *= 10;
        }
        return true;
    }
        //3797 -> 379 -> 37 -> 3
    public static boolean fromRightToLeft(int value) {
        while (value > 0) {
            if(!isPrime(value)){
                return false;
            }
            value /= 10;
        }
        return true;
    }

    public static boolean isPrime(int value) {
        if (value <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(value); i++) {
            if (value % i == 0) {
                return false;
            }
        }
        return true;
    }
}
